FROM dsnetworkapp/dsnetwork-server:v3
LABEL maintainer="Audrey Lemaçon <Audrey.lemacon@mhi-rc.org>" \
	org.label-schema.schema-version="1.0" \
	org.label-schema.version="0.0.1" \
	org.label-schema.name="dsnetwork" \
	org.label-schema.description="DSNetwork is an versatile and flexible integrative web application acting as a single-entry point to almost 60 reference predictors for both coding and non-coding variants. It aims to help the prioritization of variants by aggregating the predictions of their potential functional implications." \
	org.label-schema.url="https://www.biorxiv.org/content/10.1101/526335v1" 

# validate workdir
WORKDIR /home/root

# add score description
RUN wget -cO - https://bitbucket.org/vmtrap/dsnetwork_deploy/raw/HEAD/data/scores_description.tsv > /opt/data/scores_description.tsv


# SELECT OPTIONAL SCORES

# Extensions are the same for all scores:
# ".bed.bgz"        => bed files
# ".bed.bgz.tbi"    => tabix index file

## --- BayesDEL (540MB)
## download data and put it in /opt/data/VICTOR

#----------------------- UNCOMMENT THE FOLLOWING LINES TO GET BayesDEL
# WORKDIR /opt/data/VICTOR
# RUN wget https://adcloud.genome.ulaval.ca/index.php/s/rYNeHFcFAfLKEGP/download --output-document victor.bed.bgz
# RUN wget https://adcloud.genome.ulaval.ca/index.php/s/QNzx7pyXAPRZ7cr/download --output-document victor.bed.bgz.tbi
# WORKDIR /home/root
#-----------------------

## --- CDTS (3.5GB)
## download data and put it in /opt/data/CDTS

#----------------------- UNCOMMENT THE FOLLOWING LINES TO GET CDTS
# WORKDIR /opt/data/CDTS
# RUN wget https://adcloud.genome.ulaval.ca/index.php/s/wfbpitiw6Q2b9P9/download --output-document cdts.bed.bgz
# RUN wget https://adcloud.genome.ulaval.ca/index.php/s/xkBRkMFNHbJEdeT/download --output-document cdts.bed.bgz.tbi
# WORKDIR /home/root
#-----------------------


## --- LINSIGHT (2.2GB)
## download data and put it in /opt/data/LINSIGHT

#----------------------- UNCOMMENT THE FOLLOWING LINES TO GET LINSIGHT
# WORKDIR /opt/data/LINSIGHT
# RUN wget https://adcloud.genome.ulaval.ca/index.php/s/8a5kagaqBSdDR4c/download --output-document linsight.bed.bgz
# RUN wget https://adcloud.genome.ulaval.ca/index.php/s/Tc9PxZogt3t8AEW/download --output-document linsight.bed.bgz.tbi
# WORKDIR /home/root
#-----------------------


# Launch DSNetwork
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
