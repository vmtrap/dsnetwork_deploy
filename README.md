
# DSNetwork

## Description

This repository contains DSNetwork's source code and user guide (`user_guide.pdf`). DSNetwork can be installed according tto wo configurations : **light** and **full**. The default version, **light** (~about 2.5Go), provides all the features but LINSIGHT, BayesDel and CDTS scores, while the **full** version (~9Go) which contains all the scores. To get the **full** version, follow the optional steps of the *Download and configure* section.

## Prerequisites 
Install [Docker](https://docs.docker.com/engine/installation/) on your system.

## Download and configure

1. Download and extract, or clone, this repository.

2. Open a terminal window (Terminal application for Linux and MacOS ; Command Prompt or PowerShell, but not PowerShell ISE for Window OS).

3. Go inside the `dsnetwork-deploy` repertory.

> The following steps are optional and enable to download the supplementary scores

4. `[optional]` Open the `Dockerfile` with a text editor (suggestions: gedit for Linux, TextEdit for MacOS; Notepad++ for Windows. We strongly advise not to use Office Word)

5. `[optional]` Uncomment the lines to add optional scores `CDTS`, `LINSIGHT` and `BayesDEL`.

6. `[optional]` Save the modified file.

## Deploy DSNetwork

Inside the `dsnetwork-deploy` repertory.

1. Run `docker build . --tag="dsnetwork"` (build DSNetwork image)

> The last step can take several minutes if you add optional scores

2. Run `docker run -d -p 3838:3838 --restart on-failure dsnetwork` (run DSNetwork container)

> You can also use the `dsnetwork.sh` script that do these commands.

## Acces DSNetwork interface

Open a browser and go to [localhost:3838](http://localhost:3838).

## Stop DSNetwork container

1. Open a terminal window

2. Execute `docker ps -a`

3. Identify the `CONTAINER ID` corresponding to DSNetwork

4. Execute `docker stop [replace by the relevant container id]`

## Suppress DSNetwork image (equivalent to uninstall)

1. Open a terminal window

2. Execute `docker images`

3. Identify the `IMAGE ID` corresponding to DSNetwork

4. Execute `docker rmi [replace by the relevant image id]`

## Suggestions and Bug reports

Do not hesitate to contact us on the repository dedicated to suggestions/issues reporting: [here](https://github.com/ArnaudDroitLab/DSNETWORK)

